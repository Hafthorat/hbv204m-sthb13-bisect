package is.hi.hbv501g.team20.taeknilaesi.model;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

public class CommentTest {
    private Comment c1;
    private Comment c2;

    @BeforeEach
    void constructCourseTestObjects(){
        c1 = new Comment(1, "First Post!", "Unnur");
        c2 = new Comment(2, "Hello", "Jón");
    }

    @Test
    public void testEquals(){
        Comment equalComment = new Comment(1, "First Post!", "Unnur");
        assertTrue(c1.equals(c1));
        assertFalse(c1.equals(equalComment));
        // assertTrue(c1.getId().equals(1));
        assertTrue(c1.getUserName().equals("Unnur"));
    }
}
